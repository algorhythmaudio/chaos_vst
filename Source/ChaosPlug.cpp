//
//
// ChaosPlug.cpp
// ChaosPlug VST plug-in project
//
// Created:         2012/11/11 by Christian Floisand
// Last updated:    2012/11/12 by Christian Floisand
//
//

#include <cstdio>
#include <cstdlib>
#include "ChaosPlug.h"


//--------------------------------------------------------------------------------------------
AudioEffect* createEffectInstance (audioMasterCallback audioMaster)
{
	return new ChaosPlug(audioMaster);
}
//--------------------------------------------------------------------------------------------
ChaosPlug::ChaosPlug (audioMasterCallback audioMaster) : AudioEffectX(audioMaster, 0, kParamNum),
															LPfilter{true, true}, HPfilter{true, true},
															BPfilter{true, true}, BRfilter{true, true}
{
	// set editor for plug-in GUI
	//extern AEffGUIEditor* createEditor (AudioEffectX*);
	//setEditor (createEditor (this));
	
#ifdef DEBUG
	
	debugFile.open(kDebugLogFilename);
	DEBUGLOG("Chaos VST Plug-in Debug Mode, version " << ChaosPlugVersionStr << '\n' << '\n');
	
	char hostVendorString[kVstMaxVendorStrLen] = "";
	char hostProductString[kVstMaxProductStrLen] = "";
	if (getHostVendorString(hostVendorString))		DEBUGLOG("Host vendor: " << hostVendorString << '\n');
	if (getHostProductString(hostProductString))	DEBUGLOG("Host product: " << hostProductString << '\n');
	if (getHostVendorVersion() != 0)				DEBUGLOG("Version: " << getHostVendorVersion() << '\n');
	
#endif // DEBUG
	
	// initialize parameters to startup defaults
	sampleRate = getSampleRate();
	paramsChaos[kParam1_GlobalChaosRate] = GlobalChaosRate2float(kParam1Default);
    paramsChaos[kParam2_GlobalChaosScale] = GlobalChaosScale2float(kParam2Default);
    paramsChaos[kParam3_FilterType] = FilterType2float(kParam3Default);
    paramsChaos[kParam4_FilterQ] = FilterQ2float(kParam4Default);
    paramsChaos[kParam5_FilterChaosScale] = FilterChaosScale2float(kParam5Default);
    paramsChaos[kParam6_FilterChaosVar] = FilterChaosVar2float(kParam6Default);
    paramsChaos[kParam7_DistType] = DistortionType2float(kParam7Default);
    paramsChaos[kParam8_DistChaosScale] = DistortionChaosScale2float(kParam8Default);
    paramsChaos[kParam9_DistChaosVar] = DistortionChaosVar2float(kParam9Default);
    paramsChaos[kParam10_PanChaosScale] = PanningChaosScale2float(kParam10Default);
    paramsChaos[kParam11_PanChaosVar] = PanningChaosVar2float(kParam11Default);
	
    chaosRate = sampleRate * float2GlobalChaosRate(paramsChaos[kParam1_GlobalChaosRate]);
	
	// filter init
	paramsFilter.set(paramID_Q, float2FilterQ(paramsChaos[kParam4_FilterQ]));
	paramsFilter.set(paramID_Sr, sampleRate);
	filterL = &HPfilter[0];
	filterR = &HPfilter[1];
	filterL->setAllParams(paramsFilter);
	filterR->setAllParams(paramsFilter);
		
	distortion = waveShaper1;
	
	// set input/output to STEREO, and uniqueID
	setNumInputs(2);
	setNumOutputs(2);
	setUniqueID(CCONST('A', 'l', 'C', 's'));
	canProcessReplacing();
	resume();
}
//--------------------------------------------------------------------------------------------
void ChaosPlug::resume ()
{
	// update the sample rate before processing if needed
	if (sampleRate != updateSampleRate()) {
		sampleRate = updateSampleRate();
		filterL->setParam(paramID_Sr, sampleRate);
		filterR->setParam(paramID_Sr, sampleRate);
	}
	
    chaosModulator.setScale(static_cast<double>(float2GlobalChaosScale(paramsChaos[kParam2_GlobalChaosScale]) / 200.0));
	
	DEBUGLOG("resume()-- ");
	DEBUGLOG("Global Chaos Scale = " << float2GlobalChaosScale(paramsChaos[kParam2_GlobalChaosScale]) <<
			 ", Global Chaos Rate = " << chaosRate << '\n');
	
	// initialize filters
	filterL->reset();
	filterR->reset();
    filterL->setParam(paramID_Q, float2FilterQ(paramsChaos[kParam4_FilterQ]));
    filterR->setParam(paramID_Q, float2FilterQ(paramsChaos[kParam4_FilterQ]));
	
	DEBUGLOG("\tFilters initialized.  Q = " << float2FilterQ(paramsChaos[kParam4_FilterQ]) << '\n');
	
	// initialize distorion
    switch (float2DistortionChaosVar(paramsChaos[kParam9_DistChaosVar])) {
        case kChaosX: // x
            distAmount = static_cast<float>(fabs(chaosModulator.chaos->x) * paramsChaos[kParam8_DistChaosScale]);
            break;
        case kChaosY: // y
            distAmount = static_cast<float>(fabs(chaosModulator.chaos->y) * paramsChaos[kParam8_DistChaosScale]);
            break;
        case kChaosZ: // z
            distAmount = static_cast<float>(fabs(chaosModulator.chaos->z) * paramsChaos[kParam8_DistChaosScale]);
            break;
    }
    
    switch (float2DistortionType(paramsChaos[kParam7_DistType])) {
        default:
        case kParam7Type1:
            distortion = waveShaper1;
            distAmount /= 2.0;
            break;
        case kParam7Type2:
            distortion = waveShaper3;
            
            distAmount /= 100.0;
            if (distAmount >= 1.0)
                distAmount = 0.99;
            break;
        case kParam7Type3:
            distortion = foldDist1;
            
            distAmount /= 50.0;
            if (distAmount >= 1.0)
                distAmount = 1.0;
            break;
    }
	
	DEBUGLOG("\tDistortion initialized.  distAmount = " << distAmount << '\n');
	
	AudioEffectX::resume ();
}
//--------------------------------------------------------------------------------------------
void ChaosPlug::suspend ()
{
	AudioEffectX::suspend ();
}
//--------------------------------------------------------------------------------------------
ChaosPlug::~ChaosPlug ()
{
#ifdef DEBUG
	DEBUGLOG("Plug-in terminated." << '\n');
#endif // DEBUG
}
//--------------------------------------------------------------------------------------------
bool ChaosPlug::getEffectName (char *name)
{
	strcpy(name, "Chaos");
	return true;
}
//--------------------------------------------------------------------------------------------
bool ChaosPlug::getProductString (char *text)
{
	strcpy(text, "Chaos");
	return true;
}
//--------------------------------------------------------------------------------------------
bool ChaosPlug::getVendorString (char *text)
{
	strcpy(text, "AlgoRhythm Audio");
	return true;
}
//--------------------------------------------------------------------------------------------
// setParameter is only called by the default UI
//--------------------------------------------------------------------------------------------
void ChaosPlug::setParameter (VstInt32 index, float value)
{
	setChaosParam(index, value);
	
	if (index == kParam3_FilterType) {
		switch (float2FilterType(value)) {
			case kParam3TypeLP:
				filterL = &LPfilter[0];
				filterR = &LPfilter[1];
				DEBUGLOG("\t\t(filters set to low-pass in setParameter()" << '\n');
				break;
				
			case kParam3TypeHP:
				filterL = &HPfilter[0];
				filterR = &HPfilter[1];
				DEBUGLOG("\t\t(filters set to high-pass in setParameter()" << '\n');
				break;
				
			case kParam3TypeBP:
				filterL = &BPfilter[0];
				filterR = &BPfilter[1];
				DEBUGLOG("\t\t(filters set to band-pass in setParameter()" << '\n');
				break;
				
			case kParam3TypeBR:
				filterL = &BRfilter[0];
				filterR = &BRfilter[1];
				DEBUGLOG("\t\t(filters set to band-reject in setParameter()" << '\n');
				break;
				
			default:
				break;
		}
	}
}
//--------------------------------------------------------------------------------------------
// setParameterAutomated is called by the custom GUI editor
//--------------------------------------------------------------------------------------------
/*void ChaosPlug::setParameterAutomated (VstInt32 index, float value)
{
	switch (index) {
		case aAParam_Freq:		setFreq(value);					break;
		case aAParam_Depth:		setDepth(value);                break;
		case aAParam_Wavetype:	setWave(value);
			
			switch (float2wavetype(paramWavetype_)) {
				case aAWave_Sine:	modulator_ = &sinOsc_;					break;
				case aAWave_SawUp:	sawOsc_.setType(SAW_UP); modulator_ = &sawOsc_;		break;
				case aAWave_SawDn:	sawOsc_.setType(SAW_DOWN); modulator_ = &sawOsc_;	break;
				case aAWave_Tri:	modulator_ = &triOsc_;					break;
				case aAWave_Square:	modulator_ = &sqOsc_;
			}
	}
	
	// send parameter value to GUI editor
	if (editor)
		((AEffGUIEditor*)editor)->setParameter(index, value);
}*/
//--------------------------------------------------------------------------------------------
float ChaosPlug::getParameter (VstInt32 index)
{
	float param = paramsChaos[index];
	return param;
}
//--------------------------------------------------------------------------------------------
void ChaosPlug::getParameterName (VstInt32 index, char *label)
{
	switch (index) {
		case kParam1_GlobalChaosRate:		strcpy(label, ParamName1);		break;
		case kParam2_GlobalChaosScale:		strcpy(label, ParamName2);		break;
		case kParam3_FilterType:            strcpy(label, ParamName3);		break;
        case kParam4_FilterQ:               strcpy(label, ParamName4);		break;
        case kParam5_FilterChaosScale:      strcpy(label, ParamName5);		break;
        case kParam6_FilterChaosVar:        strcpy(label, ParamName6);		break;
        case kParam7_DistType:				strcpy(label, ParamName7);		break;
        case kParam8_DistChaosScale:        strcpy(label, ParamName8);		break;
        case kParam9_DistChaosVar:          strcpy(label, ParamName9);		break;
        case kParam10_PanChaosScale:        strcpy(label, ParamName10);		break;
        case kParam11_PanChaosVar:          strcpy(label, ParamName11);		break;
		default:							strcpy(label, "");
	}
}
//--------------------------------------------------------------------------------------------
void ChaosPlug::getParameterLabel (VstInt32 index, char *label)
{
	switch (index) {
		case kParam1_GlobalChaosRate:		strcpy(label, "");		break;
		case kParam2_GlobalChaosScale:		strcpy(label, "");		break;
		case kParam3_FilterType:            strcpy(label, "");		break;
        case kParam4_FilterQ:               strcpy(label, "");		break;
        case kParam5_FilterChaosScale:      strcpy(label, "");		break;
        case kParam6_FilterChaosVar:        strcpy(label, "");		break;
        case kParam7_DistType:				strcpy(label, "");		break;
        case kParam8_DistChaosScale:        strcpy(label, "");		break;
        case kParam9_DistChaosVar:          strcpy(label, "");		break;
        case kParam10_PanChaosScale:        strcpy(label, "");		break;
        case kParam11_PanChaosVar:          strcpy(label, "");		break;
		default:							strcpy(label, "");
	}
}
//--------------------------------------------------------------------------------------------
void ChaosPlug::getParameterDisplay (VstInt32 index, char *text)
{
	// handles parameter mapping in the default UI
	switch (index) {
		case kParam1_GlobalChaosRate:
			float2string(float2GlobalChaosRate(paramsChaos[index]), text, kVstMaxParamStrLen);
			break;
		case kParam2_GlobalChaosScale:
			float2string(float2GlobalChaosScale(paramsChaos[index]), text, kVstMaxParamStrLen);
			break;
		case kParam3_FilterType:
			switch (float2FilterType(paramsChaos[index])) {
				case kParam3TypeLP:		strcpy(text, Param3TypeNameLP);		break;
				case kParam3TypeHP:		strcpy(text, Param3TypeNameHP);		break;
				case kParam3TypeBP:		strcpy(text, Param3TypeNameBP);		break;
				case kParam3TypeBR:		strcpy(text, Param3TypeNameBR);		break;
				default:				strcpy(text, "");
			}
			break;
		case kParam4_FilterQ:
			float2string(float2FilterQ(paramsChaos[index]), text, kVstMaxParamStrLen);
			break;
		case kParam5_FilterChaosScale:
			float2string(float2FilterChaosScale(paramsChaos[index]), text, kVstMaxParamStrLen);
			break;
		case kParam6_FilterChaosVar:
			switch (float2FilterChaosVar(paramsChaos[index])) {
				case kChaosX:		strcpy(text, CHAOS_X);		break;
				case kChaosY:		strcpy(text, CHAOS_Y);		break;
				case kChaosZ:		strcpy(text, CHAOS_Z);		break;
				case kChaosNone:	strcpy(text, CHAOS_NONE);		break;
				default:			strcpy(text, "");
			}
			break;
		case kParam7_DistType:
			switch (float2DistortionType(paramsChaos[index])) {
				case kParam7Type1:		strcpy(text, Param7TypeName1);		break;
				case kParam7Type2:		strcpy(text, Param7TypeName2);		break;
				case kParam7Type3:		strcpy(text, Param7TypeName3);		break;
				default:				strcpy(text, "");
			}
			break;
		case kParam8_DistChaosScale:
			float2string(float2DistortionChaosScale(paramsChaos[index]), text, kVstMaxParamStrLen);
			break;
		case kParam9_DistChaosVar:
			switch (float2DistortionChaosVar(paramsChaos[index])) {
				case kChaosX:		strcpy(text, CHAOS_X);		break;
				case kChaosY:		strcpy(text, CHAOS_Y);		break;
				case kChaosZ:		strcpy(text, CHAOS_Z);		break;
				case kChaosNone:	strcpy(text, CHAOS_NONE);		break;
				default:			strcpy(text, "");
			}
			break;
		case kParam10_PanChaosScale:
			float2string(float2PanningChaosScale(paramsChaos[index]), text, kVstMaxParamStrLen);
			break;
		case kParam11_PanChaosVar:
			switch (float2PanningChaosVar(paramsChaos[index])) {
				case kChaosX:		strcpy(text, CHAOS_X);		break;
				case kChaosY:		strcpy(text, CHAOS_Y);		break;
				case kChaosZ:		strcpy(text, CHAOS_Z);		break;
				case kChaosNone:	strcpy(text, CHAOS_NONE);		break;
				default:			strcpy(text, "");
			}
			break;
		default:
			break;
	}
}
//--------------------------------------------------------------------------------------------
void ChaosPlug::processReplacing (float **inputs, float **outputs, VstInt32 sampleFrames)
{
	float *inL = inputs[0];
	float *inR = inputs[1];
	float *outL = outputs[0];
	float *outR = outputs[1];
	
	float panAngle;
	
#ifdef DEBUG
	DEBUGLOG("processReplacing() block-- sampleFrames = " << sampleFrames << '\n');
	DEBUGLOG("\tChaos Variables: x = " << chaosModulator.chaos->x <<
			 ", y = " << chaosModulator.chaos->y <<
			 ", z = " << chaosModulator.chaos->z << '\n');
#endif
	
	
	// process
	while (--sampleFrames >= 0) {
		
		if (chaosRate-- <= 0) {
            chaosRate = sampleRate * float2GlobalChaosRate(paramsChaos[kParam1_GlobalChaosRate]);
            chaosModulator.chaosTick();
			DEBUGLOG("\t**chaosRate reset to " << chaosRate << '\n');
        }
		
		if (chaosModulator.interpComplete == NO) {
            chaosModulator.interpChaos();
			
			switch (float2FilterChaosVar(paramsChaos[kParam6_FilterChaosVar])) {
				case kChaosX: // x
					filterL->setParam(paramID_Freq, ((chaosModulator.chaos->x * chaosModulator.chaos->x) * 20.0) *
									  float2FilterChaosScale(paramsChaos[kParam5_FilterChaosScale]));
					filterR->setParam(paramID_Freq, ((chaosModulator.chaos->x * chaosModulator.chaos->x) * 20.0) *
									  float2FilterChaosScale(paramsChaos[kParam5_FilterChaosScale]));
					break;
				case kChaosY: // y
					filterL->setParam(paramID_Freq, ((chaosModulator.chaos->y * chaosModulator.chaos->y) * 20.0) *
									  float2FilterChaosScale(paramsChaos[kParam5_FilterChaosScale]));
					filterR->setParam(paramID_Freq, ((chaosModulator.chaos->y * chaosModulator.chaos->y) * 20.0) *
									  float2FilterChaosScale(paramsChaos[kParam5_FilterChaosScale]));
					break;
				case kChaosZ: // z
					filterL->setParam(paramID_Freq, ((chaosModulator.chaos->z * chaosModulator.chaos->z) * 20.0) *
									  float2FilterChaosScale(paramsChaos[kParam5_FilterChaosScale]));
					filterR->setParam(paramID_Freq, ((chaosModulator.chaos->z * chaosModulator.chaos->z) * 20.0) *
									  float2FilterChaosScale(paramsChaos[kParam5_FilterChaosScale]));
					break;
			}
			
			// calc panning angle
			switch (float2PanningChaosVar(paramsChaos[kParam11_PanChaosVar])) {
				case kChaosX: // x
					panAngle = static_cast<float>(((2.0 * atan(1.0)) * (chaosModulator.chaos->x / 25.0)) * 0.5);
					break;
				case kChaosY: // y
					panAngle = static_cast<float>(((2.0 * atan(1.0)) * (chaosModulator.chaos->y / 25.0)) * 0.5);
					break;
				case kChaosZ: // z
					panAngle = static_cast<float>(((2.0 * atan(1.0)) * (chaosModulator.chaos->z / 25.0)) * 0.5);
					break;
				default:
					panAngle = 1;
			}
		}
		
		// filter
        if (float2FilterChaosVar(paramsChaos[kParam6_FilterChaosVar]) != kChaosNone) {
            filterL->process(inL, inL, 1);
			filterR->process(inR, inR, 1);
		}
        
        // distortion
        if (float2DistortionChaosVar(paramsChaos[kParam9_DistChaosVar]) != kChaosNone) {
            *inL = distortion(inL, distAmount);
			*inR = distortion(inR, distAmount);
		}
        
        // panning
        if (float2PanningChaosVar(paramsChaos[kParam11_PanChaosVar]) != kChaosNone) {
			*inL *= ROOT2OVR2 * (cos(panAngle) - sin(panAngle)); // left
			*inR *= ROOT2OVR2 * (cos(panAngle) + sin(panAngle)); // right
        }
		
		(*outL++) = (*inL++);
		(*outR++) = (*inR++);
		
	}
	
	DEBUGLOG("end of processReplacing() block--" << '\n' << "\tfilters' frequency = " << filterL->getParam(paramID_Freq) << '\n');
	DEBUGLOG("\tdistAmount = " << distAmount << '\n' << "\tpanAngle = " << panAngle << '\n' << '\n');
}
//--------------------------------------------------------------------------------------------

