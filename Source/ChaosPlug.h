//
//
// ChaosPlug.h
// ChaosPlug VST plug-in project
//
// Created:         2012/11/11 by Christian Floisand
// Last updated:    2012/11/12 by Christian Floisand
//
// Version:         0.1
//
//

#ifndef _ChaosPlug_h_
#define _ChaosPlug_h_

#pragma warning (disable: 4068) // disable unknown pragma warnings

#include <cstring>
#include <cmath>
#include <cassert>
#include "audioeffectx.h"

// include AA libs
#include "ChaosPlug_Version.h"
#include "Chaos_Parameters.h"
#include "AAChaos.h"
#include "AAFilterLibrary.h"
#include "AADistortion.h"

#ifdef DEBUG
#include "AAPlugDebug.h"
#include <typeinfo>
#endif

using namespace AADsp;

#pragma mark ____ChaosPlug Declaration
// -----------------------------------------------------------------------------------------------
class ChaosPlug : public AudioEffectX
{
protected:

    float   paramsChaos[kParamNum];     // chaos parameters, array indexed by enums in Chaos_Parameters.h
	float	sampleRate;
    
    AAChaos chaosModulator;
    int     chaosRate;
    
    // filters
    AAFilter::IIRBiquad<RBJ::LP> LPfilter[2];   // one for each channel in a stereo pair
    AAFilter::IIRBiquad<RBJ::HP> HPfilter[2];
    AAFilter::IIRBiquad<RBJ::BP1> BPfilter[2];
    AAFilter::IIRBiquad<RBJ::BR> BRfilter[2];
    AAFilterParams paramsFilter;                // one set of filter parameters
    AAFilter::AAFilterBase *filterL, *filterR;  // pointer to each channel in a stereo pair
    
    // Distortion ivars
    dist_sample             distortion;
    float                   distAmount;
    
public:
	// initialization and termination
	ChaosPlug (audioMasterCallback audioMaster);
	~ChaosPlug ();
	void resume ();
	void suspend ();
	bool getEffectName (char *name);
	bool getVendorString (char *text);
	bool getProductString (char *text);
	VstInt32 getVendorVersion () { return ChaosPlugVersionInt; }
	VstPlugCategory getPlugCategory () { return kPlugCategEffect; }
	
	// processing
	void processReplacing (float **inputs, float **outputs, VstInt32 sampleFrames);
	
	// parameters and programs
	void setParameter (VstInt32 index, float value);
	//void setParameterAutomated (VstInt32 index, float value);
	float getParameter (VstInt32 index);
	void getParameterLabel (VstInt32 index, char *label);
	void getParameterDisplay (VstInt32 index, char *text);
	void getParameterName (VstInt32 index, char *text);
	
	// set chaos parameter function by ID
    //---------------------------------------------------------
	void setChaosParam (int paramId, float value)
    {
        assert(paramId >= 0 && paramId < kParamNum);
        paramsChaos[paramId] = value;
    }
	
};


#endif // _ChaosPlug_h_
