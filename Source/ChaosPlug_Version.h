//
//
// ChaosPlug_Version.h
// ChaosPlug VST plug-in project
//
// Created:         2012/11/11 by Christian Floisand
// Last updated:    2012/11/11 by Christian Floisand
//
// Contains versioning information.
//
//

#ifndef _ChaosPlug_Version_h_
#define _ChaosPlug_Version_h_

#define ChaosPlugVersionInt 1
#define ChaosPlugVersionStr "Debug"

#endif // _ChaosPlug_Version_h_